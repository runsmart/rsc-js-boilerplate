# rsc-js-boilerplate
[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

A JavaScript Boilerplate for generic projects.

## Getting started

Clone this repository, remove remote from git and run npm install or simply copy and paste this command in your terminal:

```sh
git clone https://bitbucket.org/runsmart/rsc-js-boilerplate.git &&
cd rsc-js-boilerplate &&
git remote rm origin &&
npm install
```

## Resources
* Editorconfig
* ESLint
* TernJS
* StandardJS
